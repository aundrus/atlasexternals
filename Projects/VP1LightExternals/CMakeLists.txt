# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Main configuration file for building the VP1LightExternals project.
#

# The minimum required CMake version:
cmake_minimum_required( VERSION 3.6 FATAL_ERROR )

# Make sure that all _ROOT variables *are* used when they are set.
if( POLICY CMP0074 )
   cmake_policy( SET CMP0074 NEW )
endif()
# Handle CMP0072 by actively selecting the legacy OpenGL library by default.
set( OpenGL_GL_PREFERENCE "LEGACY" CACHE BOOL
   "Prefer using /usr/lib64/libGL.so unless the user says otherwise" )

# Set where to pick up AtlasCMake and AtlasLCG from.
set( AtlasCMake_DIR "${CMAKE_SOURCE_DIR}/../../Build/AtlasCMake"
   CACHE PATH "Location of AtlasCMake" )
set( LCG_DIR "${CMAKE_SOURCE_DIR}/../../Build/AtlasLCG"
   CACHE PATH "Location of AtlasLCG" )
mark_as_advanced( AtlasCMake_DIR LCG_DIR )

# Read in the project's version from a file called version.txt. But let it be
# overridden from the command line if necessary.
file( READ ${CMAKE_SOURCE_DIR}/version.txt _version )
string( STRIP ${_version} _version )
set( VP1LIGHTEXTERNALS_PROJECT_VERSION ${_version}
   CACHE STRING "Version of the VP1LightExternals project to build" )
unset( _version )

# Find the ATLAS CMake code:
find_package( AtlasCMake REQUIRED )

# Use the LCG release if found, or only the LCG modules only if not
set( LCG_VERSION_POSTFIX ""
   CACHE STRING "Post-fix for the LCG version number" )
set( LCG_VERSION_NUMBER 95
   CACHE STRING "LCG version number to use for the project" )
find_package( LCG ${LCG_VERSION_NUMBER} EXACT )
if( NOT LCG_FOUND )
   message( STATUS "Building project without relying on LCG" )
   set( LCG_DIR ${CMAKE_SOURCE_DIR}/../../Build/AtlasLCG ) # this forces CMake to "find" LCG again
   set( LCG_VERSION_POSTFIX "" )
   set( LCG_VERSION_NUMBER 0 )
   find_package( LCG ${LCG_VERSION_NUMBER} REQUIRED )
endif()

# Make CMake aware of the files in the cmake/ subdirectory.
list( INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake )

# Load the project's build options:
include( ProjectOptions )

# Set up CTest:
atlas_ctest_setup()

# Declare project name and version
atlas_project( VP1LightExternals ${VP1LIGHTEXTERNALS_PROJECT_VERSION}
   PROJECT_ROOT ${CMAKE_SOURCE_DIR}/../../ )

# Configure and install the post-configuration file:
configure_file( ${CMAKE_SOURCE_DIR}/cmake/PostConfig.cmake.in
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PostConfig.cmake @ONLY )
install( FILES ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PostConfig.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR} )

# Install the export sanitizer script:
install(
   FILES ${CMAKE_SOURCE_DIR}/cmake/install/atlas_export_sanitizer.cmake.in
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules/skeletons )

# Generate the environment setup for the externals, to be used during the build:
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )

# Generate replacement rules for the installed paths:
set( _replacements )
if( NOT "$ENV{NICOS_PROJECT_HOME}" STREQUAL "" )
   get_filename_component( _buildDir $ENV{NICOS_PROJECT_HOME} PATH )
   list( APPEND _replacements ${_buildDir}
      "\${VP1LightExternals_DIR}/../../../.." )
endif()
if( NOT "$ENV{NICOS_PROJECT_RELNAME}" STREQUAL "" )
   list( APPEND _replacements $ENV{NICOS_PROJECT_RELNAME}
      "\${VP1LightExternals_VERSION}" )
endif()

# Now generate and install the installed setup files:
lcg_generate_env(
   SH_FILE ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/env_setup_install.sh
   REPLACE ${_replacements} )
install( FILES ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/env_setup_install.sh
   DESTINATION . RENAME env_setup.sh )

# Package up the release using CPack:
atlas_cpack_setup()
