# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  BACKPORTS_ABC_PYTHON_PATH
#
# Can be steered by BACKPORTS_ABC_LCGROOT.
#

# The LCG include(s).
include( LCGFunctions )

# Find it.
lcg_python_external_module( NAME backports_abc
   PYTHON_NAMES backports_abc/__init__.py backports_abc.py )

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( backports_abc DEFAULT_MSG
   _BACKPORTS_ABC_PYTHON_PATH )

# Set up the RPM dependency.
lcg_need_rpm( backports_abc )
