#!/bin/sh
if [ "$(cat /etc/redhat-release | grep '^Scientific Linux .*6.*')" ]; then
  export OS=slc6
  export COMPILER=gcc62
elif [ "$(cat /etc/centos-release | grep 'CentOS Linux release 7')" ]; then
  export OS=centos7
  export COMPILER=gcc8
else
  echo "Unknown OS" 1>&2
fi

echo "OS and COMPILER: $OS and $COMPILER"
printenv

