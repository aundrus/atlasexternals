# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Package building nlohmann_json as part of the offline / analysis software build.
#

# The name of the package:
atlas_subdir( nlohmann_json )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# The source of nlohmann_json:
set( _source
   "http://cern.ch/atlas-software-dist-eos/externals/nlohmann/json-3.5.0.tar.gz" )
set( _md5 "3ec890e74f962854bf7d2b1d07ab4051" )

# Temporary directory for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/jsonBuild )

# Set up extra CMake cache values.
set( _extraArgs )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Build nlohmann_json for the build area:
ExternalProject_Add( nlohmann_json
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   CMAKE_CACHE_ARGS 
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DJSON_BuildTests:BOOL=false # switch off the build of the tests
   ${_extraArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( nlohmann_json purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for nlohmann_json"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( nlohmann_json buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing nlohmann_json into the build area"
   DEPENDEES install )
add_dependencies( Package_nlohmann_json nlohmann_json )

# And now install it:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
