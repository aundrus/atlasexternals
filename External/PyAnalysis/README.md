Python Analysis Modules
=======================

This package collects some Python modules useful for analysis.
The name "PyAnalysis" sort of mimicks the "pyanalysis" package name from LCG.

Currently the package basically just provides NumPy for the standalone
analysis release. The package is only meant to be used in AnalysisBase, at
least for now. Since LCG provides an even larger set of analysis modules,
so all projects building on top of LCG should just be using the modules from
there.
