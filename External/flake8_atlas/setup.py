# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Register flake8 plugins:
#   http://flake8.pycqa.org/en/latest/plugin-development/registering-plugins.html
#
import setuptools
from flake8_atlas import __version__

requires = [
   "flake8 > 3.0.0",
]

setuptools.setup(
   name="flake8_atlas",
   version=__version__,
   description="ATLAS plugins for flake8",
   author='Frank Winklmeier',
   url='https://gitlab.cern.ch/atlas/atlasexternals/tree/master/External/flake8_atlas',
   test_loader='unittest:TestLoader',
   packages=['flake8_atlas'],
   zip_safe=True,
   entry_points={
      'flake8.extension': [
         'ATL100 = flake8_atlas.checks:delayed_string_interpolation',
         'ATL231 = flake8_atlas.python23:hacking_python3x_octal_literals',
         'ATL232 = flake8_atlas.python23:incompatible_print_statement',
         'ATL233 = flake8_atlas.python23:print_statement',
         'ATL234 = flake8_atlas.python23:hacking_no_assert_equals',
         'ATL235 = flake8_atlas.python23:hacking_no_assert_underscore',
         'ATL236 = flake8_atlas.python23:hacking_python3x_metaclass',
         'ATL237 = flake8_atlas.python23:hacking_no_removed_module',
         'ATL238 = flake8_atlas.python23:no_old_style_class',
         'ATL900 = flake8_atlas.checks:OutputLevel',
         'ATL901 = flake8_atlas.checks:print_for_logging',
         'ATL902 = flake8_atlas.checks:Copyright',
      ],
   }
)
