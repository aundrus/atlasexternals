# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
from flake8_atlas.test.testutils import Flake8Test

class Test(Flake8Test):
   """
   Test old-style class check
   """

   def test_class(self):
      """Test old-style class check"""
      from flake8_atlas.python23 import no_old_style_class as checker
      self.assertFail('class K:', checker)
      self.assertFail('class K():', checker)
      self.assertFail('  class   K   :', checker)
      self.assertPass('class K(object):', checker)
      self.assertPass('  class KLASS   (object)  :', checker)
      self.assertPass('classifier = K(', checker)
