# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration file describing the build and installation of the
# Coin3D-simage library.
#

# The name of the package:
atlas_subdir( Simage )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SimageBuild )

# The sources:
set( _source
   "http://cern.ch/atlas-software-dist-eos/externals/Simage/Coin3D-simage-2c958a61ea8b.zip" )
set( _md5 "7440b33776519ce19f536018a2adea3f" )

# Build the library for the build area:
ExternalProject_Add( Simage
   PREFIX ${CMAKE_BINARY_DIR}
   
   URL ${_source}
   URL_MD5 ${_md5}
   
   CONFIGURE_COMMAND ./configure --prefix=${_buildDir}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   BUILD_IN_SOURCE 1
   PATCH_COMMAND patch -p1 <
   ${CMAKE_CURRENT_SOURCE_DIR}/patches/libpng_centos7.patch
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
ExternalProject_Add_Step( Simage forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of Simage"
   DEPENDERS download )
add_dependencies( Package_Simage Simage )

# Install the package:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
