# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Package building SoQt for ATLAS.
#

# The package's name:
atlas_subdir( Coin3D )

# Set up its package dependencies:
atlas_depends_on_subdirs(
   PRIVATE Externals/Simage )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/coin3dBuild )

# The sources:
set( _source
   "http://cern.ch/atlas-software-dist-eos/externals/Coin3D/coin_e74da18.zip" )
set( _md5 "931c52af660b7df88ded1070567d990c" )

# Extra CMake arguments.
set( _extraArgs )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Set up the build of Coin3D, according to the OS
# NOTE:
# in order to let CMake find the custom Coin3D instead of the LCG one, we
# need to pass the path '${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}' to
# -DCMAKE_PREFIX_PATH
#
ExternalProject_Add( Coin3D
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   CMAKE_CACHE_ARGS
   -DSIMAGE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DSIMAGE_RUNTIME_LINKING:BOOL=ON
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DCOIN_BUILD_DOCUMENTATION:BOOL=false
   ${_extraArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Coin3D buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing Coin3D into the build area"
   DEPENDEES install  )
add_dependencies( Coin3D Simage )
add_dependencies( Package_Coin3D Coin3D )

# Install Coin3D:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
